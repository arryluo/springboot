/*
Navicat MySQL Data Transfer

Source Server         : blo
Source Server Version : 50173
Source Host           : 47.94.254.90:3306
Source Database       : blgmanager

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2018-02-28 16:52:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for fujian
-- ----------------------------
DROP TABLE IF EXISTS `fujian`;
CREATE TABLE `fujian` (
  `id` varchar(100) NOT NULL,
  `uid` varchar(100) DEFAULT NULL,
  `picurl` varchar(200) DEFAULT NULL,
  `createtime` varchar(50) DEFAULT NULL,
  `danjuid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fujian
-- ----------------------------
INSERT INTO `fujian` VALUES ('22332', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1505750120884.jpg', '2018-01-21', '123');
INSERT INTO `fujian` VALUES ('d4c391528ddd4fb68291f0864fb2b230', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1516641002151.jpg', '2018-01-21 22:00:13', 'cdb5ceb25e324401a79a62491aa0e75c');
INSERT INTO `fujian` VALUES ('7e5ede366dbf457abb2f22fb450bd549', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1516636320148.jpg', '2018-01-22 21:37:49', '2d1a0d93a263482caabc5b1319d47fd3');
INSERT INTO `fujian` VALUES ('efda08c9e9cf45d69008b1bc6477e4b3', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1516701498533.jpg', '2018-01-22 22:22:27', '19b3aef849bc4a47b2653a552eaa1c72');
INSERT INTO `fujian` VALUES ('f5f897158e8b4a89b52699f95b4be9a6', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1517772472636.jpg', '2018-02-04 13:43:41', '2ff608f168fa4c65a00ea04746615d46');
INSERT INTO `fujian` VALUES ('8a009603213d4ad99c63ca3380cff6f9', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1517732360388.jpg', '2018-02-04 13:50:17', '5a01f80886cc428a8117192a7477a54d');
INSERT INTO `fujian` VALUES ('3758c69c23b647b1af4e968ce2b2fa50', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1517797580819.jpg', '2018-02-04 13:55:33', 'e2cfe00bae794430a185ddf2af35552a');
INSERT INTO `fujian` VALUES ('cc88aece898940a19a54ef9628a9903f', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1517806291958.jpg', '2018-02-04 13:55:33', 'e2cfe00bae794430a185ddf2af35552a');
INSERT INTO `fujian` VALUES ('52bba9e2c8dc40febc741f8b7596d12e', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1517785609470.jpg', '2018-02-04 14:00:27', '444ab25d30a64401814026cb78cd4e7a');
INSERT INTO `fujian` VALUES ('75a9176427584268af4b800b30b96f36', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1517777383959.jpg', '2018-02-04 14:00:27', '444ab25d30a64401814026cb78cd4e7a');
INSERT INTO `fujian` VALUES ('cd86ca9a993d4316b3fb497e109abc2d', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1517758864365.jpg', '2018-02-04 14:11:54', '33512f5d94a1417488291b6071242c31');
INSERT INTO `fujian` VALUES ('6fd60f8be3e64c849e74bd17b10f4ded', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1517827904911.jpg', '2018-02-04 16:23:46', '7d54872fe19045c4a3907cfb431fc687');
INSERT INTO `fujian` VALUES ('72e68910c7e24993a54eab94b796d976', '1', 'http://ov51d64rm.bkt.clouddn.com/1517849523784.jpg', '2018-02-05 09:25:11', '6010879d34f84313a753d3f6e9999acd');
INSERT INTO `fujian` VALUES ('d8b9936a0bb84e56b02c7f143b361a7d', '', 'http://ov51d64rm.bkt.clouddn.com/1518237743499.jpg', '2018-02-09 19:29:22', '8d7325d5a1654fe2b62c8f6322cac601');
INSERT INTO `fujian` VALUES ('1c4b29ac5a4f4aceb86d9d1d743330a4', '29bd880c1c7d4c258ab90f910aefd6c6', 'http://ov51d64rm.bkt.clouddn.com/1518188365613.jpg', '2018-02-09 19:38:50', 'd4bfbfe1366649b4b99b019ffc7d8ccd');
