/*
Navicat MySQL Data Transfer

Source Server         : blo
Source Server Version : 50173
Source Host           : 47.94.254.90:3306
Source Database       : blgmanager

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2018-02-28 16:52:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dianzan
-- ----------------------------
DROP TABLE IF EXISTS `dianzan`;
CREATE TABLE `dianzan` (
  `id` varchar(100) NOT NULL,
  `danjuid` varchar(100) DEFAULT NULL,
  `uid` varchar(100) DEFAULT NULL,
  `uname` varchar(50) DEFAULT NULL,
  `createtime` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dianzan
-- ----------------------------
INSERT INTO `dianzan` VALUES ('954595bde8cf4201982d0d6121d0fe34', '23db6146a8144982a73d16b11d5d4fe4', '29bd880c1c7d4c258ab90f910aefd6c6', '136***4834', '2018-02-10 15:26:16');
INSERT INTO `dianzan` VALUES ('d7cbe400ffdb45049174507a47b6c6f6', '6890b46ad6c54a3fb217c008b42bcc85', '29bd880c1c7d4c258ab90f910aefd6c6', '136***4834', '2018-02-09 23:40:42');
INSERT INTO `dianzan` VALUES ('8023f9d4bb494cab9b0739f885e08316', 'd4bfbfe1366649b4b99b019ffc7d8ccd', '29bd880c1c7d4c258ab90f910aefd6c6', '136***4834', '2018-02-28 13:01:56');
INSERT INTO `dianzan` VALUES ('21c0fdf246f0426588ebd5a504d8f8e0', 'c646d9e23ff641d98891052773e8cb1d', '29bd880c1c7d4c258ab90f910aefd6c6', '136***4834', '2018-02-28 13:01:46');
