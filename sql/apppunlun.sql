/*
Navicat MySQL Data Transfer

Source Server         : blo
Source Server Version : 50173
Source Host           : 47.94.254.90:3306
Source Database       : blgmanager

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2018-02-28 16:51:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for apppunlun
-- ----------------------------
DROP TABLE IF EXISTS `apppunlun`;
CREATE TABLE `apppunlun` (
  `id` varchar(100) NOT NULL,
  `uid` varchar(100) DEFAULT NULL,
  `content` varchar(1000) DEFAULT NULL,
  `createtime` varchar(50) DEFAULT NULL,
  `danjuid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of apppunlun
-- ----------------------------
INSERT INTO `apppunlun` VALUES ('a10f6f27d1084b6899cf6ce04468fb69', '29bd880c1c7d4c258ab90f910aefd6c6', '神速', '2018-02-09 19:41:35', 'cb227585b8314007b6d50b2896c36317');
INSERT INTO `apppunlun` VALUES ('8c3c4f527a0a482a81d34c91ba19dd4b', '29bd880c1c7d4c258ab90f910aefd6c6', '可以的', '2018-02-10 09:44:00', '23db6146a8144982a73d16b11d5d4fe4');
INSERT INTO `apppunlun` VALUES ('b8bbe9f737674be693de78ba31d4e9fd', '29bd880c1c7d4c258ab90f910aefd6c6', '来了', '2018-02-10 09:44:15', 'd4bfbfe1366649b4b99b019ffc7d8ccd');
INSERT INTO `apppunlun` VALUES ('f2e25c37d1ae4b2e83d468b97884c08a', '29bd880c1c7d4c258ab90f910aefd6c6', 'www', '2018-02-10 09:44:21', 'cb227585b8314007b6d50b2896c36317');
INSERT INTO `apppunlun` VALUES ('c5b11f5b8b2b41b0baa48abf402ca11f', '29bd880c1c7d4c258ab90f910aefd6c6', '家里', '2018-02-26 21:16:27', '23db6146a8144982a73d16b11d5d4fe4');

-- ----------------------------
-- Table structure for apppunlun_d
-- ----------------------------
DROP TABLE IF EXISTS `apppunlun_d`;
CREATE TABLE `apppunlun_d` (
  `id` varchar(100) NOT NULL,
  `content` varchar(1000) DEFAULT NULL,
  `createtime` varchar(50) DEFAULT NULL,
  `danjuid` varchar(100) DEFAULT NULL,
  `zid` varchar(100) DEFAULT NULL COMMENT '被回复人',
  `uid` varchar(100) DEFAULT NULL COMMENT '回复人',
  `flgtype` tinyint(4) DEFAULT NULL COMMENT '用于标识是回复人',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of apppunlun_d
-- ----------------------------
INSERT INTO `apppunlun_d` VALUES ('be4777f66ed24964a8ff3227f05679d9', '解决了', '2018-02-09 19:58:06', 'a10f6f27d1084b6899cf6ce04468fb69', '29bd880c1c7d4c258ab90f910aefd6c6', '67a6c017749d469caca6b0bf6c6e9c67', '1');
INSERT INTO `apppunlun_d` VALUES ('e106cc33b0c74c23b35bf1c3f3b0900b', '神速', '2018-02-26 21:16:19', 'a10f6f27d1084b6899cf6ce04468fb69', '67a6c017749d469caca6b0bf6c6e9c67', '29bd880c1c7d4c258ab90f910aefd6c6', '1');
INSERT INTO `apppunlun_d` VALUES ('2c66143dcf3b47a48048d880c91b4268', 'dsad', '2018-02-28 13:02:16', 'a10f6f27d1084b6899cf6ce04468fb69', '67a6c017749d469caca6b0bf6c6e9c67', '29bd880c1c7d4c258ab90f910aefd6c6', '1');
INSERT INTO `apppunlun_d` VALUES ('7370a4da418e40c2901f6c5b147cdda8', '我爱你啊', '2018-02-28 13:02:39', 'a10f6f27d1084b6899cf6ce04468fb69', '67a6c017749d469caca6b0bf6c6e9c67', '29bd880c1c7d4c258ab90f910aefd6c6', '1');

-- ----------------------------
-- Table structure for dianzan
-- ----------------------------
DROP TABLE IF EXISTS `dianzan`;
CREATE TABLE `dianzan` (
  `id` varchar(100) NOT NULL,
  `danjuid` varchar(100) DEFAULT NULL,
  `uid` varchar(100) DEFAULT NULL,
  `uname` varchar(50) DEFAULT NULL,
  `createtime` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dianzan
-- ----------------------------
INSERT INTO `dianzan` VALUES ('954595bde8cf4201982d0d6121d0fe34', '23db6146a8144982a73d16b11d5d4fe4', '29bd880c1c7d4c258ab90f910aefd6c6', '136***4834', '2018-02-10 15:26:16');
INSERT INTO `dianzan` VALUES ('d7cbe400ffdb45049174507a47b6c6f6', '6890b46ad6c54a3fb217c008b42bcc85', '29bd880c1c7d4c258ab90f910aefd6c6', '136***4834', '2018-02-09 23:40:42');
INSERT INTO `dianzan` VALUES ('8023f9d4bb494cab9b0739f885e08316', 'd4bfbfe1366649b4b99b019ffc7d8ccd', '29bd880c1c7d4c258ab90f910aefd6c6', '136***4834', '2018-02-28 13:01:56');
INSERT INTO `dianzan` VALUES ('21c0fdf246f0426588ebd5a504d8f8e0', 'c646d9e23ff641d98891052773e8cb1d', '29bd880c1c7d4c258ab90f910aefd6c6', '136***4834', '2018-02-28 13:01:46');

-- ----------------------------
-- Table structure for dongtai
-- ----------------------------
DROP TABLE IF EXISTS `dongtai`;
CREATE TABLE `dongtai` (
  `id` varchar(100) NOT NULL,
  `uid` varchar(100) DEFAULT NULL,
  `content` text COMMENT '内容',
  `createtime` varchar(50) DEFAULT NULL COMMENT '发布时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dongtai
-- ----------------------------
INSERT INTO `dongtai` VALUES ('cb227585b8314007b6d50b2896c36317', '29bd880c1c7d4c258ab90f910aefd6c6', 'dsad', '2018-02-09 19:35:51');
INSERT INTO `dongtai` VALUES ('d4bfbfe1366649b4b99b019ffc7d8ccd', '29bd880c1c7d4c258ab90f910aefd6c6', '我很爱你啊', '2018-02-09 19:38:50');
INSERT INTO `dongtai` VALUES ('c646d9e23ff641d98891052773e8cb1d', '29bd880c1c7d4c258ab90f910aefd6c6', '解决了', '2018-02-09 23:41:03');
INSERT INTO `dongtai` VALUES ('23db6146a8144982a73d16b11d5d4fe4', '29bd880c1c7d4c258ab90f910aefd6c6', '解决', '2018-02-10 09:03:38');

-- ----------------------------
-- Table structure for fujian
-- ----------------------------
DROP TABLE IF EXISTS `fujian`;
CREATE TABLE `fujian` (
  `id` varchar(100) NOT NULL,
  `uid` varchar(100) DEFAULT NULL,
  `picurl` varchar(200) DEFAULT NULL,
  `createtime` varchar(50) DEFAULT NULL,
  `danjuid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fujian
-- ----------------------------
INSERT INTO `fujian` VALUES ('22332', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1505750120884.jpg', '2018-01-21', '123');
INSERT INTO `fujian` VALUES ('d4c391528ddd4fb68291f0864fb2b230', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1516641002151.jpg', '2018-01-21 22:00:13', 'cdb5ceb25e324401a79a62491aa0e75c');
INSERT INTO `fujian` VALUES ('7e5ede366dbf457abb2f22fb450bd549', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1516636320148.jpg', '2018-01-22 21:37:49', '2d1a0d93a263482caabc5b1319d47fd3');
INSERT INTO `fujian` VALUES ('efda08c9e9cf45d69008b1bc6477e4b3', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1516701498533.jpg', '2018-01-22 22:22:27', '19b3aef849bc4a47b2653a552eaa1c72');
INSERT INTO `fujian` VALUES ('f5f897158e8b4a89b52699f95b4be9a6', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1517772472636.jpg', '2018-02-04 13:43:41', '2ff608f168fa4c65a00ea04746615d46');
INSERT INTO `fujian` VALUES ('8a009603213d4ad99c63ca3380cff6f9', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1517732360388.jpg', '2018-02-04 13:50:17', '5a01f80886cc428a8117192a7477a54d');
INSERT INTO `fujian` VALUES ('3758c69c23b647b1af4e968ce2b2fa50', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1517797580819.jpg', '2018-02-04 13:55:33', 'e2cfe00bae794430a185ddf2af35552a');
INSERT INTO `fujian` VALUES ('cc88aece898940a19a54ef9628a9903f', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1517806291958.jpg', '2018-02-04 13:55:33', 'e2cfe00bae794430a185ddf2af35552a');
INSERT INTO `fujian` VALUES ('52bba9e2c8dc40febc741f8b7596d12e', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1517785609470.jpg', '2018-02-04 14:00:27', '444ab25d30a64401814026cb78cd4e7a');
INSERT INTO `fujian` VALUES ('75a9176427584268af4b800b30b96f36', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1517777383959.jpg', '2018-02-04 14:00:27', '444ab25d30a64401814026cb78cd4e7a');
INSERT INTO `fujian` VALUES ('cd86ca9a993d4316b3fb497e109abc2d', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1517758864365.jpg', '2018-02-04 14:11:54', '33512f5d94a1417488291b6071242c31');
INSERT INTO `fujian` VALUES ('6fd60f8be3e64c849e74bd17b10f4ded', '3c0caf4c4b8f476697fd91ab170047ba', 'http://ov51d64rm.bkt.clouddn.com/1517827904911.jpg', '2018-02-04 16:23:46', '7d54872fe19045c4a3907cfb431fc687');
INSERT INTO `fujian` VALUES ('72e68910c7e24993a54eab94b796d976', '1', 'http://ov51d64rm.bkt.clouddn.com/1517849523784.jpg', '2018-02-05 09:25:11', '6010879d34f84313a753d3f6e9999acd');
INSERT INTO `fujian` VALUES ('d8b9936a0bb84e56b02c7f143b361a7d', '', 'http://ov51d64rm.bkt.clouddn.com/1518237743499.jpg', '2018-02-09 19:29:22', '8d7325d5a1654fe2b62c8f6322cac601');
INSERT INTO `fujian` VALUES ('1c4b29ac5a4f4aceb86d9d1d743330a4', '29bd880c1c7d4c258ab90f910aefd6c6', 'http://ov51d64rm.bkt.clouddn.com/1518188365613.jpg', '2018-02-09 19:38:50', 'd4bfbfe1366649b4b99b019ffc7d8ccd');

-- ----------------------------
-- Table structure for users_d
-- ----------------------------
DROP TABLE IF EXISTS `users_d`;
CREATE TABLE `users_d` (
  `id` varchar(100) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `pwd` varchar(100) DEFAULT NULL,
  `img` varchar(200) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `createtime` varchar(100) NOT NULL,
  `nickname` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users_d
-- ----------------------------
INSERT INTO `users_d` VALUES ('2', '17671683602', '123456', 'http://ov51d64rm.bkt.clouddn.com/1505750143567.jpg', null, '2017-11-13 02:10:43', '呵呵哒');
INSERT INTO `users_d` VALUES ('29bd880c1c7d4c258ab90f910aefd6c6', '13617144834', '123456', 'http://ov51d64rm.bkt.clouddn.com/1505093063103.jpg', null, '2018-02-09 19:19:01', '136***4834');
INSERT INTO `users_d` VALUES ('3c0caf4c4b8f476697fd91ab170047ba', '18186330729', '123456', 'http://ov51d64rm.bkt.clouddn.com/1505750172138.jpg', null, '2017-12-09 21:15:08', '123456');
INSERT INTO `users_d` VALUES ('67a6c017749d469caca6b0bf6c6e9c67', '18606513169', '123456', 'http://ov51d64rm.bkt.clouddn.com/1505750172138.jpg', null, '2017-10-29 15:48:09', '而我却');
INSERT INTO `users_d` VALUES ('8732bbc6b140454a88497510ecf95505', '13477661487', '123456', 'http://ov51d64rm.bkt.clouddn.com/1505750133669.jpg', null, '2017-12-09 21:17:30', 'dd');
INSERT INTO `users_d` VALUES ('ba20bc9e61294dc1b7ef0c7f4dd966b4', '15658001790', '123456', 'http://ov51d64rm.bkt.clouddn.com/1505750143567.jpg', null, '2017-11-06 09:57:00', '逸馨');
INSERT INTO `users_d` VALUES ('c24057bd970d49318ecb9fe586511d2b', '15161464540', 'chen12345', 'http://ov51d64rm.bkt.clouddn.com/1505750144353.jpg', null, '2017-12-31 20:21:12', '野鸽');
INSERT INTO `users_d` VALUES ('c51a7dbca6a04eaca9133f9ba9491ea7', '13635852469', '123456', 'http://ov51d64rm.bkt.clouddn.com/1505750143567.jpg', null, '2017-11-13 02:10:43', 'Ra1nDr0p');
