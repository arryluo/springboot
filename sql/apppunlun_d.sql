/*
Navicat MySQL Data Transfer

Source Server         : blo
Source Server Version : 50173
Source Host           : 47.94.254.90:3306
Source Database       : blgmanager

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2018-02-28 16:52:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for apppunlun_d
-- ----------------------------
DROP TABLE IF EXISTS `apppunlun_d`;
CREATE TABLE `apppunlun_d` (
  `id` varchar(100) NOT NULL,
  `content` varchar(1000) DEFAULT NULL,
  `createtime` varchar(50) DEFAULT NULL,
  `danjuid` varchar(100) DEFAULT NULL,
  `zid` varchar(100) DEFAULT NULL COMMENT '被回复人',
  `uid` varchar(100) DEFAULT NULL COMMENT '回复人',
  `flgtype` tinyint(4) DEFAULT NULL COMMENT '用于标识是回复人',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of apppunlun_d
-- ----------------------------
INSERT INTO `apppunlun_d` VALUES ('be4777f66ed24964a8ff3227f05679d9', '解决了', '2018-02-09 19:58:06', 'a10f6f27d1084b6899cf6ce04468fb69', '29bd880c1c7d4c258ab90f910aefd6c6', '67a6c017749d469caca6b0bf6c6e9c67', '1');
INSERT INTO `apppunlun_d` VALUES ('e106cc33b0c74c23b35bf1c3f3b0900b', '神速', '2018-02-26 21:16:19', 'a10f6f27d1084b6899cf6ce04468fb69', '67a6c017749d469caca6b0bf6c6e9c67', '29bd880c1c7d4c258ab90f910aefd6c6', '1');
INSERT INTO `apppunlun_d` VALUES ('2c66143dcf3b47a48048d880c91b4268', 'dsad', '2018-02-28 13:02:16', 'a10f6f27d1084b6899cf6ce04468fb69', '67a6c017749d469caca6b0bf6c6e9c67', '29bd880c1c7d4c258ab90f910aefd6c6', '1');
INSERT INTO `apppunlun_d` VALUES ('7370a4da418e40c2901f6c5b147cdda8', '我爱你啊', '2018-02-28 13:02:39', 'a10f6f27d1084b6899cf6ce04468fb69', '67a6c017749d469caca6b0bf6c6e9c67', '29bd880c1c7d4c258ab90f910aefd6c6', '1');
