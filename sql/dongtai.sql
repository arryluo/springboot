/*
Navicat MySQL Data Transfer

Source Server         : blo
Source Server Version : 50173
Source Host           : 47.94.254.90:3306
Source Database       : blgmanager

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2018-02-28 16:52:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dongtai
-- ----------------------------
DROP TABLE IF EXISTS `dongtai`;
CREATE TABLE `dongtai` (
  `id` varchar(100) NOT NULL,
  `uid` varchar(100) DEFAULT NULL,
  `content` text COMMENT '内容',
  `createtime` varchar(50) DEFAULT NULL COMMENT '发布时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dongtai
-- ----------------------------
INSERT INTO `dongtai` VALUES ('cb227585b8314007b6d50b2896c36317', '29bd880c1c7d4c258ab90f910aefd6c6', 'dsad', '2018-02-09 19:35:51');
INSERT INTO `dongtai` VALUES ('d4bfbfe1366649b4b99b019ffc7d8ccd', '29bd880c1c7d4c258ab90f910aefd6c6', '我很爱你啊', '2018-02-09 19:38:50');
INSERT INTO `dongtai` VALUES ('c646d9e23ff641d98891052773e8cb1d', '29bd880c1c7d4c258ab90f910aefd6c6', '解决了', '2018-02-09 23:41:03');
INSERT INTO `dongtai` VALUES ('23db6146a8144982a73d16b11d5d4fe4', '29bd880c1c7d4c258ab90f910aefd6c6', '解决', '2018-02-10 09:03:38');
