/*
Navicat MySQL Data Transfer

Source Server         : blo
Source Server Version : 50173
Source Host           : 47.94.254.90:3306
Source Database       : blgmanager

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2018-02-28 16:53:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for users_d
-- ----------------------------
DROP TABLE IF EXISTS `users_d`;
CREATE TABLE `users_d` (
  `id` varchar(100) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `pwd` varchar(100) DEFAULT NULL,
  `img` varchar(200) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `createtime` varchar(100) NOT NULL,
  `nickname` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users_d
-- ----------------------------
INSERT INTO `users_d` VALUES ('2', '17671683602', '123456', 'http://ov51d64rm.bkt.clouddn.com/1505750143567.jpg', null, '2017-11-13 02:10:43', '呵呵哒');
INSERT INTO `users_d` VALUES ('29bd880c1c7d4c258ab90f910aefd6c6', '13617144834', '123456', 'http://ov51d64rm.bkt.clouddn.com/1505093063103.jpg', null, '2018-02-09 19:19:01', '136***4834');
INSERT INTO `users_d` VALUES ('3c0caf4c4b8f476697fd91ab170047ba', '18186330729', '123456', 'http://ov51d64rm.bkt.clouddn.com/1505750172138.jpg', null, '2017-12-09 21:15:08', '123456');
INSERT INTO `users_d` VALUES ('67a6c017749d469caca6b0bf6c6e9c67', '18606513169', '123456', 'http://ov51d64rm.bkt.clouddn.com/1505750172138.jpg', null, '2017-10-29 15:48:09', '而我却');
INSERT INTO `users_d` VALUES ('8732bbc6b140454a88497510ecf95505', '13477661487', '123456', 'http://ov51d64rm.bkt.clouddn.com/1505750133669.jpg', null, '2017-12-09 21:17:30', 'dd');
INSERT INTO `users_d` VALUES ('ba20bc9e61294dc1b7ef0c7f4dd966b4', '15658001790', '123456', 'http://ov51d64rm.bkt.clouddn.com/1505750143567.jpg', null, '2017-11-06 09:57:00', '逸馨');
INSERT INTO `users_d` VALUES ('c24057bd970d49318ecb9fe586511d2b', '15161464540', 'chen12345', 'http://ov51d64rm.bkt.clouddn.com/1505750144353.jpg', null, '2017-12-31 20:21:12', '野鸽');
INSERT INTO `users_d` VALUES ('c51a7dbca6a04eaca9133f9ba9491ea7', '13635852469', '123456', 'http://ov51d64rm.bkt.clouddn.com/1505750143567.jpg', null, '2017-11-13 02:10:43', 'Ra1nDr0p');
