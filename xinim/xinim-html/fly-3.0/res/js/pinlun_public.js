var uids="";
(function($) {
    //获取要回复人的username，以及uid
    var huname = "";
    var huid = "";
    //存储点赞时的点赞列表人员名称
    //存储当前用户的信息
    var nickname="";
    var user_id="";
    var img="";
    var namelists = [];
    var apppinlun = {
        init: function(parent) {
            //首先加载的是列表
            //apppinlun.donglist(parent);
            //执行数据的请求入口
            apppinlun.initpost(parent);
        },
        //点赞的添加
        adddianzan:function (danjuid,uid,uname,flgtype) {
            var json={
                uid:uid,
                danjuid:danjuid,
                uname:uname,
                flgtype:flgtype
            }
            // console.log("12");
            //console.log(json)
            apppinlun.initexecute("/dainzan",json,"2","","");
        },
        //对一级评论的一个添加
        oneadd:function (id,content,danjuid,typeflg,zid) {
            var json={
                uid:id,
                danjuid:danjuid,
                content:content,
                typeflg:typeflg,
                zid:zid,
            }
            //console.log(json);
            apppinlun.initexecute("/addpinlun",json,"2","","");
        },
        /*	//二级评论,id是当前人的id，danjuid是被回复人的id
            twoadd:function (id,content,danjuid) {
                var json={
                    uid:id,
                    danjuid:danjuid,
                    content:content,
                    typeflg:"2"
                }
                apppinlun.initexecute("/appmanager/addpinlun.do",json,"2","","");
            },*/
        //列表的加载数据
        listdata:function (page,next) {
            var json={
                pageno:page,
                uid:""
            }
            apppinlun.initexecute("/dtlist",json,"1",next,page);
        },
        initfilldata:function (parent,jsdata) {
            switch (parent.mode){
                case "0":
                    //执行数据列表的加载操作
                    apppinlun. donglist(parent,jsdata);
                    console.log(jsdata);
                    break
                case "1":
                    //在这里进行数据的集成
                    var pagecount=jsdata.pagecount;
                    var next=parent.next;
                    var page=parent.pageno;
                    //获取用户名
                    user_id=jsdata.users.id;//当前登录用户的id
                    uids=user_id;
                    nickname=jsdata.users.nickname;
                    img=jsdata.users.img;
                    //在这里对跳入添加页面的数据进行赋值
                    $(".layui-fixbar").attr("uid",user_id);
                    setTimeout(function() {
                        var lis = [];
                        for(var i = 0; i <jsdata.data.length; i++) {
                            var html = apppinlun.datalisthtml(jsdata.data[i]);
                            lis.push(html)
                        }

                        //执行下一页渲染，第二参数为：满足“加载更多”的条件，即后面仍有分页
                        //pages为Ajax返回的总页数，只有当前页小于总页数的情况下，才会继续出现加载更多
                        next(lis.join(''), page < pagecount); //假设总页数为 10
                        //popimg();
                        //在这里进行处理所以有的点击事件
                        //暂时没有数据交互时就放到这里
                        //然后填充各种的点击事件
                        //图片的点击事件
                        apppinlun.imgpopclick(parent);
                        //apppinlun.imgpop(parent);
                        apppinlun.topclickhuifu(parent); //在文章下面的一个回复按钮的点击事件
                        apppinlun.qitaclick(parent); //点击其他地方进行
                        apppinlun.pinlunsave(parent, "1"); //评论保存
                        apppinlun.ontpinlun(parent); //一级评论
                        //取消
                        apppinlun.cenlpinlun(parent);
                        //对一级评论进行评论的点击进入ui
                        apppinlun.towclickui(parent);
                        apppinlun.dianzan(parent);
                        apppinlun.threeclickui(parent); //第三级评论的添加
                    }, 500);
                    break;
            }


        },
        //图片的点击事件
        imgpopwindow: function(parent, imgpw) {
            layui.use('layer', function() {
                var layer = layui.layer;
                layer.photos({
                    photos:imgpw,
                    anim: 5, //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
                    tab: function(pic) {
                        //console.log(pic);
                    }
                });

            });
        },
        //图片的点击事件
        imgpopclick:function(parent){
            $("#LAY_demo1 .imgpop").off().on("click",function(){
                var imgpw=$(this).parent(".site-demo-flow");
                apppinlun.imgpopwindow(parent,imgpw);
            });
        },
        //加载列表，用流的形式加载
        donglist: function(parent,jsdata) {
            layui.use('flow', function() {
                var flow = layui.flow;
                flow.load({
                    elem: '#LAY_demo1' //流加载容器

                    ,
                    done: function(page, next) { //执行下一页的回调
                        //var sum = 0;
                        console.log(page);
                        //模拟数据插入
                        console.log("1");
                        //如果page=1的时候，不执行加载方法
                        //var pagecount=jsdata.pagecount;
                        //if(page!=1){
                        apppinlun.listdata(page,next);

                        //}


                    }
                });
            });
        },
        //进行点赞操作
        dianzan: function(parent) {
            //当前的点赞往上找对象
            $(".icon-zan").off().on("click", function() {
                //获取当前单击的id
                var danjuid=$(this).parents(".danjuitem").attr("id");
                //首先获取点赞的数量
                /*var numbers=$(this).children("em").text();
                var sum=0;
                if(numbers.trim()!=''){
                    sum=parseInt(numbers);
                }*/
                var _this = $(this).parent(".jieda-zan");
                namelists = []; //数据滞空
                var sum = apppinlun.reflashnumber(_this);
                //console.log(sum);
                //var numbse=0;
                /*if(sum!=''){
                    numbse=parseInt(sum);
                }*/

                var sw = _this.attr("sw");
                var demos = _this.parents(".layui-colla-content").parent("div").children(".pinlunlistdata").children(".dianzanlist");
                demos.children().remove();
                if(sw == 'off') {
                    //说明是没有点赞的
                    _this.css("color", "#c00");
                    _this.attr("sw", "on");
                    sum++;
                    //进行异步的请求
                    apppinlun.adddianzan(danjuid,user_id,nickname,"1");
                    if(namelists.length==0){
                        demos.append('<i class="iconfont icon-zan"></i>');
                        demos.append(apppinlun.dianzanhtml(nickname+"、")); //当前的用户名
                    }
                    $.each(namelists, function(i, d) {
                        if(i == 0) {
                            demos.append('<i class="iconfont icon-zan"></i>');
                            demos.append(apppinlun.dianzanhtml(nickname+"、")); //当前的用户名
                            demos.append(apppinlun.dianzanhtml(d.name));
                        } else if(i <= 11) {
                            demos.append(apppinlun.dianzanhtml(d.name));

                        }
                        if(i == namelists.length - 1) {
                            var html = '<span href class="fly-link"><cite><font size="2.5">等' + sum + '人点赞</font></cite></span>';
                            demos.append(html);
                        }

                    });
                    //进行点赞
                    //dianzanhtml
                } else {
                    _this.css("color", "#999");
                    _this.attr("sw", "off");
                    sum--;
                    apppinlun.adddianzan(danjuid,user_id,nickname,"2");
                    //console.log(namelists)
                    for(var i = 0; i < namelists.length; i++) {
                        var index = (namelists[i].name).indexOf(nickname+"、");
                        if(index == 0) {
                            //当你取消的时候，就把自己过滤掉
                            continue;
                        }

                        if(i == 1) {
                            demos.append('<i class="iconfont icon-zan"></i>');

                            demos.append(apppinlun.dianzanhtml(namelists[i].name));

                        } else if(i <= 11) {

                            demos.append(apppinlun.dianzanhtml(namelists[i].name));

                        }
                        if(i == namelists.length - 1) {
                            var html = '<span href class="fly-link"><cite><font size="2.5">等' + sum + '人点赞</font></cite></span>';
                            demos.append(html);
                        }
                    }

                }
                //console.log(sum);
                //.jieda-reply .jieda-zan:hover{color:#c00}
            });
        },
        //实时刷新点赞的数量
        reflashnumber: function(_this) {
            var numbsers = 0;
            _this.parents(".layui-colla-content").parent("div").children(".pinlunlistdata").children(".dianzanlist").children("a").each(function(i, d) {
                namelists.push({
                    name: $(d).text()
                })
                numbsers++;
            });
            //console.log(namelists);
            return numbsers;
        },
        //对二级评论的一个回复
        threeclickui: function(parent) {
            $(".threeiconfont").off().on("click", function(e) {
                //console.log("123");
                e.stopPropagation();
                huname = $(this).attr("uname");
                //获取要回复人的uid
                var h_uid=$(this).attr("uid");
                //获取当前一级评论的id
                var h_danjuid=$(this).attr("danjuid");
                if(h_uid==user_id){
                    layer.msg("不能自己回复自己");
                    return false;
                }
                //然后将外层中的那个输入框进行移除
                $(this).parents(".pinlunlistdata").next().remove();
                //在此之前将第一个移除掉
                $(this).parents(".detail-hits").parent(".detail-about").next().find(".fly-list-info").remove();
                $(this).parents(".detail-hits").parent(".detail-about").after(apppinlun.pinluninput("threelayui-textarea"));
                $(this).parents(".twopldiv").children(".fly-list-info").children(".icon-pinglun1").children(".layui-textarea").attr("h_uid",h_uid);
                $(this).parents(".twopldiv").children(".fly-list-info").children(".icon-pinglun1").children(".layui-textarea").attr("h_danjuid",h_danjuid);
                $(this).parents(".twopldiv").children(".fly-list-info").children(".icon-pinglun1").children(".layui-textarea").attr("placeholder", "@"+huname);
                //第三级的添加操作
                apppinlun.threelayclick(parent);
            });
        },
        //对第三级中的输入框的一个点击事件
        threelayclick: function(parent) {
            $(".threelayui-textarea").off().on("click", function(e) {
                $(this).next().remove()

                $(this).after(apppinlun.btnhtml());
                $(this).css("min-height", "100px");
                e.stopPropagation();
                apppinlun.pinlunsave(parent, "3"); //保存
            });
        },
        //对一级评论进行评论的点击进入ui
        towclickui: function(parent) {
            $(".towiconfont").off().on("click", function(e) {
                e.stopPropagation();
                huname = $(this).attr("uname");
                //$(this).parents(".onepl").next().show();
                //加载输入框
                //获取要回复人的uid
                var h_uid=$(this).attr("uid");
                //获取当前一级评论的id
                var h_danjuid=$(this).attr("danjuid");
                if(h_uid==user_id){
                    layer.msg("不能自己回复自己");
                    return false;
                }
                $(this).parents(".onepl").next().remove();
                $(this).parents(".onepl").after(apppinlun.pinluninput("twolayui-textarea"));
                //然后将外层中的那个输入框进行移除
                $(this).parents(".pinlunlistdata").next().remove();

                $(this).parents(".onepl").next().children(".icon-pinglun1").children(".layui-textarea").attr("h_uid",h_uid);
                $(this).parents(".onepl").next().children(".icon-pinglun1").children(".layui-textarea").attr("h_danjuid",h_danjuid);
                $(this).parents(".onepl").next().children(".icon-pinglun1").children(".layui-textarea").attr("placeholder", "@"+huname);
                //$(".onelayui-textarea").next().remove(); //先将他
                //$(".onelayui-textarea").hide();
                apppinlun.twoclickAddBtn(parent);

            })
        },
        //对回复进行追加添加按钮
        twoclickAddBtn: function(parent) {
            $(".twolayui-textarea").off().on("click", function(e) {
                $(this).next().remove()

                $(this).after(apppinlun.btnhtml());
                $(this).css("min-height", "100px");
                e.stopPropagation();
                apppinlun.pinlunsave(parent, "2"); //保存
                //save(); //保存
                //cenl(); //取消
            });
        },
        //第一个一级评论的加载
        ontpinlun: function(parent) {
            $(".onelayui-textarea").off().on("click", function(e) {
                $(this).next().remove();

                $(this).after(apppinlun.btnhtml());
                $(this).css("min-height", "100px");
                e.stopPropagation();
                apppinlun.pinlunsave(parent, "1"); //保存
                //取消
                apppinlun.cenlpinlun(parent);
                //save(); //保存
                //cenl(); //取消
            });
        },

        //点击内容下的回复按钮进行弹出评论
        topclickhuifu: function(parent) {
            $(".tophuifu").off().on("click", function(e) {
                //$(this).parents(".layui-colla-content").parent("div").children(".fly-list-info").children(".iconfont").children(".layui-textarea").next().remove();

                $(this).parents(".layui-show").parent("div").children(".pinlunlistdata").next().remove();
                //这一步就是将在pinlunlistdata列表下的所有的输入框都给移除掉
                $(this).parents(".layui-show").parent("div").children(".pinlunlistdata").children(".pinlunlistid").find(".fly-list-info").remove();
                //第一步，加载输入框
                //apppinlun.pinluninput();
                $(this).parents(".layui-show").parent("div").children(".pinlunlistdata").after(apppinlun.pinluninput("onelayui-textarea"));
                //同时把添加按钮给添加进来
                $(this).parents(".layui-colla-content").parent("div").children(".fly-list-info").children(".iconfont").children(".layui-textarea").after(apppinlun.btnhtml());
                $(this).parents(".layui-colla-content").parent("div").children(".fly-list-info").children(".iconfont").children(".layui-textarea").css("min-height", "100px");
                $(this).parents(".layui-colla-content").parent("div").children(".fly-list-info").children(".iconfont").children(".layui-textarea").focus();
                apppinlun.pinlunsave(parent, "1"); //保存
                //$(".pinlunlistid .fly-list-info").hide(); //先将他
                //$(".pinlunlistid .fly-list-info .onelayui-textarea").next().remove();
                //save();
                //cenl(); //取消
                e.stopPropagation();
                //取消
                apppinlun.cenlpinlun(parent);

            });
        },
        //对回复按钮的一个保存操作
        pinlunsave: function(parent, type) {

            layui.use('layer', function() {
                var layer = layui.layer;

                $(".layui-col-md8 .onesave").off().on("click", function(e) {
                    e.stopPropagation();
                    //找到单据的id
                    var danjuid=$(this).parents(".danjuitem").attr("id");
                    var h_uid=$(this).parent(".layui-input-block").prev().attr("h_uid");
                    var h_danjuid=$(this).parent(".layui-input-block").prev().attr("h_danjuid");
                    //h_danjuid
                    if(typeof(h_danjuid)=="undefined"){
                        h_danjuid="";
                    }
                    //当时而已评论的时候，改上一级的内容id是undfind
                    if(typeof(h_uid)!='undefined'){
                        danjuid=h_danjuid;
                    }

                    var content = $(this).parents(".fly-list-info").children(".icon-pinglun1").children(".layui-textarea").val();
                    $(this).parents(".fly-list-info").children(".icon-pinglun1").children(".layui-textarea").val("");
                    $(this).parents(".fly-list-info").children(".icon-pinglun1").children(".layui-textarea").attr("placeholder", "请输入内容");
                    //nickname
                    //var username = "马云",
                    //uimg = "http://127.0.0.1:8020/APP/fly-3.0/res/images/a.jpg";
                    var _this = $(this);
                    var index = layer.load(0, {
                        shade: false
                    }); //0代表加载的风格，支持0-2
                    var html = "";

                    //用追加的形式进行添加

                    setTimeout(function() {
                        switch(type) {
                            case "1":
                                //这个说明是一级评论的追加内容
                                html = apppinlun.onthtml(parent, content, nickname, img);
                                _this.parents(".fly-list-info").prev(".layui-colla-content").children(".pinlunlistid").append(html);
                                apppinlun.towclickui(parent);
                                apppinlun.ontpinlun(parent); //一级评论
                                //一级评论的添加
                                apppinlun.oneadd(user_id,content,danjuid,"1","");
                                break;
                            case "2":
                                html = apppinlun.twohtml(parent, content, nickname, img, huname);
                                _this.parents(".fly-list-info").prev(".onepl").children(".detail-about").children(".twopldiv").append(html);
                                //对一级评论进行评论的点击进入ui
                                apppinlun.towclickui(parent);
                                apppinlun.threeclickui(parent); //第三级评论的添加
                                apppinlun.oneadd(user_id,content,danjuid,"2",h_uid);
                                //然后进行对其输入框隐藏掉
                                //_this.parents(".fly-list-info").hide();
                                //对一级回复之后就要对整个输入框进行移除操作
                                _this.parents(".fly-list-info").remove();
                                break;
                            case "3":
                                html = apppinlun.twohtml(parent, content,  nickname, img, huname);
                                //console.log("33");
                                _this.parents(".fly-list-info").prev(".detail-about").after(html);
                                apppinlun.oneadd(user_id,content,danjuid,"2",h_uid);
                                //对一级评论进行评论的点击进入ui
                                apppinlun.towclickui(parent);
                                apppinlun.threeclickui(parent); //第三级评论的添加
                                _this.parents(".fly-list-info").remove();
                                break;
                        }
                        //_this.parents(".fly-list-info").hide();
                        $(".layui-input-block").remove();
                        $(".layui-textarea").css("min-height", "20px");
                        layer.close(index);
                    }, 1000)

                });
            });

        },
        //动态数据列表,dataobj单个对象，用接口返回的数据
        datalisthtml: function(dataobj) {
            var html = '<li class="danjuitem" id="'+dataobj.id+'" style="background-color: #E8E8E8">' +
                '<div>' +
                '<a href="#" class="fly-avatar">' +
                '<img src="'+dataobj.uimg+'" alt="'+dataobj.username+'">' +
                '</a>' +
                '<h2>' +
                '<a >  <cite style="color: #403e3b;">'+dataobj.username+'</cite><br/>' +

                '<span style="position: absolute;  padding-right: 0!important;margin-top: 6px;">' +

                '<cite style="font-style: italic; color: #999; "><font size="2.5">'+dataobj.createtime+'</font></cite>' +
                '</span>' +
                '</a>' +

                '</h2>' +
                '</div>' +
                '<div style="margin-top: 60px;">' +
                '<div class="layui-colla-content layui-show">' +
                '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+dataobj.content+'' +

                '<div class="site-demo-flow" id="LAY_demo3" style="margin-top: 5px;">';
            /*附件的遍历*/
            for(var i=0;i<dataobj.fujian.length;i++){
                var imgpws=
                    '<img class="imgpop" src="'+dataobj.fujian[i].picurl+'">';
                html+=imgpws;
            }

            html+='</div>' +
                '<!--进行点赞的处理-->' +
                '<div class="jieda-reply">';
            var dianzan="";
            if(dataobj.dianzan=='2'){
                dianzan='<span class="jieda-zan zanok" type="zan" sw="off">';
            }else{
                dianzan='<span class="jieda-zan zanok" type="zan" style="color:#c00 " sw="on">';
            }

            html+=dianzan;
            html+='<i class="iconfont icon-zan"></i>' +
                '<!-- <em>66</em>-->' +
                '</span>' +
                '<span type="reply" class="tophuifu">' +
                '<i class="iconfont icon-svgmoban53"></i>' +
                '回复' +
                '</span>' +

                '</div>' +
                '</div>' +
                '<!--在这里编写评论列表-->' +
                '<div class="layui-colla-content layui-show pinlunlistdata" style="padding:5px 15px">' +
                '<!--点赞列表-->' +
                '<div class="dianzanlist">' +
                '<i class="iconfont icon-zan"></i>';
            for(var k=0;k<dataobj.dianzanlist.length;k++){
                var dianzanobj= dataobj.dianzanlist[k];
                var dianzanlist='<a href class="fly-link">' +
                    '<cite><font size="2.5">'+dianzanobj.uname+'、</font></cite>' +
                    '</a>';
                html+=dianzanlist;
            }
            html+='<span href class="fly-link"><cite><font size="2.5">';
            if(dataobj.dianzansize!=0){
                var dianzansum= '等'+dataobj.dianzansize+'人点赞</font></cite></span>';
                html+=dianzansum;
            }
            html+='</div>' +
            '<ul class="pinlunlistid">';
            /*循环评论*/
            for(var k=0;k<dataobj.pinlunlist.length;k++){
                var onepinlundata= dataobj.pinlunlist[k];
                var pinlun='<li class="onepl">'+
                    /*在这里面进行循环评论列表*/
                    '<div class="detail-about detail-about-reply">'+
                    '<a class="fly-avatar" href="" ><img src="'+onepinlundata.img+'" class="layui-nav-img" style="margin-right:4px;width: 30px;height: 30px;" /></a>'+
                    '<div class="detail-hits">'+
                    '<span class="fly-link"><cite>'+onepinlundata.nickname+'</cite><span style="padding-left: 5px; color: #000000;">:</span></span>'+
                    '<span style="color: #000000;">'+onepinlundata.content+'</span>'+
                    '<span type="reply" class="huifu"><i class="iconfont towiconfont" uname="'+onepinlundata.nickname+'" uid="'+onepinlundata.uid+'" danjuid="'+onepinlundata.id+'" title="回复" style="cursor: pointer;"></i></span>'+
                    '</div>'+
                    '<div class="detail-hits" style="margin-bottom: 5px;display: none">'+
                    '<span>'+onepinlundata.createtime+'</span>'+
                    '<span type="reply" class="huifu"><i class="iconfont towiconfont" uname="'+onepinlundata.nickname+'" uid="'+onepinlundata.uid+'" danjuid="'+onepinlundata.id+'" title="回复" style="cursor: pointer;"></i></span>'+
                    '</div>'+
                    '<!--一级评论结束-->'+
                    '<div class="twopldiv">';
                /*循环二级评论*/
                for(var n=0;n<onepinlundata.apppunlun_ds.length;n++){
                    var twodata=onepinlundata.apppunlun_ds[n];

                    var towhtml='<div class="detail-about detail-about-reply" style="margin-top: 5px;">'+
                        '<a class="fly-avatar" href="" ><img src="'+twodata.m_img+'" class="layui-nav-img" style="margin-right:4px;width: 30px;height: 30px;" /></a>'+
                        '<div class="detail-hits">'+
                        '<span class="fly-link"><cite>'+twodata.m_name+'&nbsp;<span style="color: #000000;">回复</span>'+twodata.z_name+'</cite><span style="padding-left: 5px; color: #000000;">:</span></span>'+
                        '<span style="color: #000000;">'+twodata.content+'</span>'+
                        '<span type="reply" class="huifu"><i class="iconfont threeiconfont" title="回复" uname="'+twodata.m_name+'" uid="'+twodata.mid+'" danjuid="'+onepinlundata.id+'" style="cursor: pointer;"></i></span>'+

                        '</div>'+
                        '<div class="detail-hits" style="margin-bottom: 10px;margin-top: 0px;margin-left: 50px;display: none">'+
                        '<span>'+twodata.createtime+'</span>'+
                        '<span type="reply" class="huifu"><i class="iconfont threeiconfont" title="回复" uname="'+twodata.m_name+'" uid="'+twodata.mid+'" danjuid="'+onepinlundata.id+'" style="cursor: pointer;"></i></span>'+
                        '</div>'+
                        '</div>';
                    pinlun+=towhtml;
                }
                pinlun+='</div>'+
                    '</div>'+
                    '</li>';
                html+=pinlun;
            }

            html+='<!--第二个人评论-->' +

                '</ul>' +
                '</div>' +

                '</div>' +
                '</li>';
            return html;
        },
        //对点赞列表进行追加html
        dianzanhtml: function(uname) {
            var html = '<a href class="fly-link"><cite><font size="2.5">' + uname + '</font></cite></a>';
            return html;
        },
        //将点击输入框时，加载出添加，取消按钮
        btnhtml: function() {
            var html = '<div class="layui-input-block">' +
                '<button class="layui-btn onesave" lay-submit=""  lay-filter="demo1">立即提交</button>' +
                '<button type="reset" class="layui-btn layui-btn-primary">取消</button></div>';
            return html;
        },
        //对评论的输入框一个追加操作
        pinluninput: function(tyclass) {
            var html = '<div class="fly-list-info">' +
                '<i class="iconfont icon-pinglun1" title="评论" style="cursor: pointer;">' +
                '<textarea placeholder="请输入内容" class="layui-textarea ' + tyclass + '"></textarea>' +

                '</i>' +

                '</div>'
            return html;
        },
        //对二级评论的一个追加hname 是被回复者
        twohtml: function(parent, content, username, uimg, hname) {
            var html = '<div class="detail-about detail-about-reply" style="margin-top: 5px;">' +
                '<a class="fly-avatar" href="" >' +
                '<img src="' + uimg + '" class="layui-nav-img" style="margin-right:4px;width: 30px;height: 30px;">' +
                '</a>' +
                '<div class="detail-hits">' +
                '<span class="fly-link">' +
                '<cite>' + username + '&nbsp;<span style="color: #000000;">回复</span>' + hname + '</cite><span style="padding-left: 5px; color: #000000;">:</span>' +
                '</span>' +
                '<span style="color: #000000;">' + content + '</span>' +
                '<span type="reply" class="huifu">' +
                '<i class="iconfont threeiconfont" title="回复" uname="' + username + '" style="cursor: pointer;"></i>' +
                '</span>' +
                '</div>' +
                '<div class="detail-hits" style="margin-bottom: 10px;margin-top: 0px;margin-left: 50px;display: none">' +
                '<span>'+apppinlun.getNowFormatDate()+'</span>' +
                '<span type="reply" class="huifu">' +
                '<i class="iconfont threeiconfont" title="回复" uname="' + username + '" style="cursor: pointer;"></i>' +
                '</span>' +
                '</div>' +
                '</div>';
            return html;
        },
        //对一级评论追加html
        onthtml: function(parent, content, username, uimg) {
            var html = '<li class="onepl">' +
                '<!--一级评论开始-->' +
                '<div class="detail-about detail-about-reply">' +
                '<a class="fly-avatar" href="" >' +
                '<img src="' + uimg + '" class="layui-nav-img" style="margin-right:4px;width: 30px;height: 30px;">' +
                '</a>' +
                '<div class="detail-hits">' +
                '<span class="fly-link">' +
                '<cite>' + username + '</cite><span style="padding-left: 5px; color: #000000;">:</span>' +

                '</span>' +

                '<span style="color: #000000;">' + content + '</span>' +
                '<span type="reply" class="huifu">' +
                '<i class="iconfont towiconfont" uname="' + username + '" title="回复" style="cursor: pointer;"></i>' +
                '</span>' +
                '</div>' +
                '<div class="detail-hits" style="margin-bottom: 5px;display: none">' +
                '<span>'+apppinlun.getNowFormatDate()+'</span>' +
                '<span type="reply" class="huifu">' +
                '<i class="iconfont towiconfont" uname="' + username + '" title="回复" style="cursor: pointer;"></i>' +
                '</span>' +
                '</div>' +
                '<!--一级评论结束-->' +
                '<div class="twopldiv">' +

                '</div>' +
                '</div>' +
                '<!--一级评论开始结束-->' +

                '</li>';
            return html;
        },
        //获取当前系统时间
        getNowFormatDate: function() {
            var date = new Date();
            var seperator1 = "-";
            var seperator2 = ":";
            var month = date.getMonth() + 1;
            var strDate = date.getDate();
            if(month >= 1 && month <= 9) {
                month = "0" + month;
            }
            if(strDate >= 0 && strDate <= 9) {
                strDate = "0" + strDate;
            }
            var h = date.getHours();
            var d = date.getMinutes();
            if(h >= 0 && h <= 9) {
                h = "0" + h;
            }
            if(d >= 0 && d <= 9) {
                d = "0" + d;
            }
            var s = date.getSeconds();
            if(s >= 0 && s <= 9) {
                s = "0" + s;
            }
            //var h=date.getHours() ;
            //console.log(h);
            // var d= date.getMinutes();
            //console.log(d);
            var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate +
                " " + h + seperator2 + d +
                seperator2 + s;
            return currentdate;
        },
        //点击其他地方进行收起
        qitaclick: function(parent) {
            //点击其他地方进行收起\n
            $(".layui-colla-content").off().on("click", function(e) {
                $(".fly-list-info").remove();
                /*	$(".layui-input-block").remove();
                    $(".layui-textarea").css("min-height", "20px");
                    $(".layui-textarea").attr("placeholder", "请输入内容");*/
                e.stopPropagation();
            });
        },
        cenlpinlun: function(parent) {
            $(".layui-btn-primary").off().on("click", function(e) {
                e.stopPropagation();
                //console.log("22");
                $(".layui-input-block").remove();
                $(".layui-textarea").css("min-height", "20px");
            });
        },
        //数据交互
        initpost: function(parent) {
            $.ajax({
                url:url+parent.url,
                data:parent.data,
                type:"POST",
                dataType:"JSON",
                success:function (obj) {
                    //填充数据
                    apppinlun.initfilldata(parent,obj);

                }
            })
        },
        //执行回调
        initexecute: function(url,data,mode,next,pageno) {
            pinluncallback({
                url:url,
                data:data,
                mode:mode,
                next:next,
                pageno:pageno,
                callback: function (obj) {

                }

            });
        }
    }
    //执行回调的方法
    pinluncallback = function(opetion) {
        var parent = $.extend({
            url: "",
            data: "",
            mode:"",
            next:"",
            pageno:"",
            callback: function(obj) {

            }
        }, opetion);
        apppinlun.init(parent);
    }

}(jQuery))