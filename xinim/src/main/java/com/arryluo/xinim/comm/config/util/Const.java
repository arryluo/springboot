package com.arryluo.xinim.comm.config.util;

/**
 * arryluo
 */
public class Const {
	public static final  String INIT_REDIS="initredis";//springmvc加载之后获取redis实例
	public static final String IMG_PATH="d:/img.txt";

	//设置rediskey的过期时间///1000*60*60*3
	public static final  int SCENT_TIME=1000*60*60*3;//过期时间3个小时
	//登录的session
	public static final  String SESSION_USER="users";
	//验证码的key
	public static final String VERIFCATION_CODE="CODE";
	//当编辑器上传图片时存入redis的key
	public static final String EDIT_IMGPATH="edit_imgpath";

}
