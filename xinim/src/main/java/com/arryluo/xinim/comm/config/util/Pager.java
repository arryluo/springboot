package com.arryluo.xinim.comm.config.util;

import java.util.List;

public class Pager<T> {

	private int firstIndex;
	private int lastIndex;
	private int pageNo = 1;
	private int pageSize = 5;
	private int totalRecordCount;
	private int totalPageCount;
	private List<T> list;
	public int getFirstIndex() {

		return firstIndex = (this.getPageNo() -1) * this.getPageSize();
	}
	
	public int getLastIndex() {
		return lastIndex = getFirstIndex() + this.getPageSize() -1;
	}
	
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getTotalRecordCount() {
		return totalRecordCount;
	}
	public void setTotalRecordCount(int totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}
	public int getTotalPageCount() {
		return totalPageCount = (this.getTotalRecordCount()-1)/this.getPageSize()+1;
	}
	public List<T> getList() {
		return list;
	}
	public void setList(List<T> list) {
		this.list = list;
	}
	
	
	
}
