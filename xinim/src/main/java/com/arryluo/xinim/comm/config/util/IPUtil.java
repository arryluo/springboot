package com.arryluo.xinim.comm.config.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by LUOZUBANG on 2017/9/6.
 */
public class IPUtil {
    /**
     * 获取当前ip
     */
    public static String getIp(){
        InetAddress inetAddress=null;
        try {
             inetAddress=InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return inetAddress.getHostAddress();
    }

}
