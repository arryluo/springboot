package com.arryluo.xinim.comm.config.util;



/**
 * Created by arryluo on 2017/8/13.
 * 对运行时异常的处理
 */
public class ExceptionUtil extends RuntimeException{

    public ExceptionUtil(String name){
        super(name);
       // log.se
    LoggerUtils.error(ExceptionUtil.class,name);
    }
}
