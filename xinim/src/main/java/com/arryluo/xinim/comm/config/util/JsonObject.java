package com.arryluo.xinim.comm.config.util;

import java.util.HashMap;
import java.util.Map;

public class JsonObject extends HashMap<String, Object> {
	private static final long serialVersionUID = 1L;

	public JsonObject() {
		put("code", 0);
		put("msg", "操作成功");
	}

	public static JsonObject error() {
		return error(1, "操作失败");
	}

	public static JsonObject error(String msg) {
		return error(500, msg);
	}

	public static JsonObject error(int code, String msg) {
		JsonObject r = new JsonObject();
		r.put("code", code);
		r.put("msg", msg);
		return r;
	}

	public static JsonObject ok(Object object) {
		JsonObject r = new JsonObject();
		r.put("msg", object);
		return r;
	}

	public static JsonObject ok(Map<String, Object> map) {
		JsonObject r = new JsonObject();
		r.putAll(map);
		return r;
	}

	public static JsonObject ok() {
		return new JsonObject();
	}

	@Override
	public JsonObject put(String key, Object value) {
		super.put(key, value);
		return this;
	}
}
