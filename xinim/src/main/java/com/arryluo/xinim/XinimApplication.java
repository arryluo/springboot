package com.arryluo.xinim;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.web.servlet.ServletComponentScan;

import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement


@ServletComponentScan
@SpringBootApplication
@MapperScan("com.arryluo.xinim.*.dao")
public class XinimApplication{

	public static void main(String[] args) {
		SpringApplication.run(XinimApplication.class, args);
	}
}
