package com.arryluo.xinim.system.mode;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by LUOZUBANG on 2018/1/29.
 //App的评论
 */
public class Apppunlun implements Serializable {
    private String id;
    private String uid;
    private String content;
    private String createtime;
    private String danjuid;
    private String img;
    private String nickname;
    //存储二级评论列表
    private List<Map<String,Object>>apppunlun_ds;

    public List<Map<String, Object>> getApppunlun_ds() {
        return apppunlun_ds;
    }

    public void setApppunlun_ds(List<Map<String, Object>> apppunlun_ds) {
        this.apppunlun_ds = apppunlun_ds;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getDanjuid() {
        return danjuid;
    }

    public void setDanjuid(String danjuid) {
        this.danjuid = danjuid;
    }
}
