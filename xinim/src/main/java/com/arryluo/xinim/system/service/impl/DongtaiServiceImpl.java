package com.arryluo.xinim.system.service.impl;

import com.arryluo.xinim.comm.config.util.DateUtil;
import com.arryluo.xinim.comm.config.util.Pager;
import com.arryluo.xinim.comm.config.util.UuidUtil;
import com.arryluo.xinim.system.dao.*;
import com.arryluo.xinim.system.mode.*;
import com.arryluo.xinim.system.service.DongtaiService;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by LUOZUBANG on 2018/1/21.
 */
@Service
public class DongtaiServiceImpl implements DongtaiService {
    @Autowired
    private DongtaiMapper dongtaiMapper;
    @Autowired
    private FujianMapper fujianMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private AppPinlunMapper appPinlunMapper;
    @Autowired
    private AppPinlun_DMapper appPinlun_dMapper;
    @Autowired
    private DianZanMapper dianZanMapper;
    @Override
    public Map<String, Object> dongtailist(int pageno,int pagesize,Users us) {
        Pager<Dongtai> pager=new Pager<Dongtai>();
        pager.setPageNo(pageno);
        pager.setPageSize(pagesize);
        pager.setTotalRecordCount(dongtaiMapper.count());
         List<Dongtai>dongtais= dongtaiMapper.selectdongtai(pager);
         Map<String,Object>mapobj=new HashedMap();
        List<Map<String,Object>>mapList=new ArrayList<Map<String, Object>>();
        for (int i = 0; i <dongtais.size() ; i++) {
           Dongtai dongtai=dongtais.get(i);
           Users users=userMapper.userinfo(dongtai.getUid());
           Map<String,Object>map=new HashedMap();
           map.put("id",dongtai.getId());
           map.put("uid",dongtai.getUid());
           map.put("content",dongtai.getContent());
           map.put("createtime",dongtai.getCreatetime());
           //根据单据的id来查询当前的点赞数据
           List<DianZan>dianZans= dianZanMapper.dianzanbydanjuid(dongtai.getId());
          /* Map<String,Object>dianzanmap=new HashedMap();
           dianzanmap.put("danjuid",dongtai.getId());
           dianzanmap.put("uid",us.getId());*/
            //L////ist<DianZan> dianZan=dianZanMapper.dianjuinfo(dianzanmap);
           /*if(dianZan.size()>0){
               map.put("dianzan","1");
           }else{
               map.put("dianzan","2");
           }*/
           //遍历取出点赞列表，取出当前登录人是否点过赞
           for (int j = 0; j <dianZans.size() ; j++) {
               DianZan dianZan=dianZans.get(j);
                if(us.getId().equals(dianZan.getUid())){
                    //点赞
                    map.put("dianzan","1");
                }else{
                    //没有点赞
                    map.put("dianzan","2");
                }
            }
            if(dianZans.size()<=0){
                //没有点赞
                map.put("dianzan","2");
            }
            map.put("dianzansize",dianZans.size());
            map.put("dianzanlist",dianZans);
           List<Fujian>fujians= fujianMapper.selectAll(dongtai.getId());
           //评论一级
           List<Apppunlun>pinLunList= appPinlunMapper.selectbydatanjuid(dongtai.getId());
         try{
             map.put("uid",users.getId());
             map.put("username",users.getNickname());
             map.put("uimg",users.getImg());
         }catch (Exception e){

         }

           //遍历一级评论列表
            List<Apppunlun>onePinlunList=new ArrayList();
            for (int j = 0; j <pinLunList.size() ; j++) {
                Apppunlun apppunlun= pinLunList.get(j);
                List<Apppunlun_D> apppunlun_ds=appPinlun_dMapper.selectbyoneid(apppunlun.getId());
                apppunlun.setApppunlun_ds(todata(apppunlun_ds));
                onePinlunList.add(apppunlun);
            }

           map.put("fujian",fujians);
           map.put("pinlunlist",onePinlunList);
           mapList.add(map);
        }
        //获取当前人的吗，名称，图片，id
        mapobj.put("users",us);
        mapobj.put("data",mapList);
        mapobj.put("pagecount",pager.getTotalPageCount());
        return mapobj;
    }
    //抽取二级id中的评论人和被评论人的人员信息
    private List<Map<String,Object>>todata( List<Apppunlun_D>apppunlun_ds){
            List<Map<String,Object>>maps=new ArrayList<Map<String, Object>>();
        for (int i = 0; i <apppunlun_ds.size() ; i++) {
           Apppunlun_D apppunlun_d=apppunlun_ds.get(i);
          Users users_z= userMapper.userinfo(apppunlun_d.getZid());//这个是被回复者的人员信息
           Users users_h= userMapper.userinfo(apppunlun_d.getUid());//这个是回复者的人员信息
            Map<String,Object>map=new HashedMap();
            map.put("id",apppunlun_d.getId());
            map.put("content",apppunlun_d.getContent());
            map.put("createtime",apppunlun_d.getCreatetime());
            map.put("flgtype",apppunlun_d.getFlgtype());
            map.put("z_name",users_z.getNickname());
            map.put("zid",apppunlun_d.getZid());
            map.put("zimg",users_z.getImg());
            map.put("mid",apppunlun_d.getUid());//这个是回复人的id
            map.put("m_name",users_h.getNickname());
            map.put("m_img",users_h.getImg());
            maps.add(map);
        }
        return maps;
    }


    @Override
    public void savedongtai(Dongtai dongtai,String imglist) {
        String did= UuidUtil.get32UUID();
        dongtai.setId(did);
        dongtai.setCreatetime(DateUtil.getTime());
        dongtaiMapper.savedongtai(dongtai);
        if(!StringUtils.isEmpty(imglist)){
           String[]imgs=imglist.split(",");
            for (int i = 0; i <imgs.length ; i++) {
                Fujian fujian=new Fujian();
                fujian.setId(UuidUtil.get32UUID());
                fujian.setCreatetime(DateUtil.getTime());
                fujian.setDanjuid(did);
                fujian.setPicurl(imgs[i]);
                fujian.setUid(dongtai.getUid());
                fujianMapper.savefujian(fujian);
            }

        }

    }

    @Override
    public Map<String, Object> pinlunlist(String id) {
      List<Apppunlun>pinLunList= appPinlunMapper.selectbydatanjuid(id);
        Map<String,Object>map=new HashedMap();
        map.put("data",pinLunList);
        return map;
    }
}
