package com.arryluo.xinim.system.dao;

import com.arryluo.xinim.system.mode.Users;


import java.util.List;

/**
 * Created by LUOZUBANG on 2017/8/13.
 */
public interface UserMapper {
    List<Users>all();
    Users login(Users users);
    Users onlyName(String username);
    void save(Users users);
    List<Users>selectallbyniect();
    Users userinfo(String id);
}

