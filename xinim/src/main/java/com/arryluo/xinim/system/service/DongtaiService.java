package com.arryluo.xinim.system.service;

import com.arryluo.xinim.system.mode.Dongtai;
import com.arryluo.xinim.system.mode.Users;


import java.util.Map;

/**
 * Created by LUOZUBANG on 2018/1/21.
 */
public interface DongtaiService {
    Map<String,Object>dongtailist(int fistindex, int pagesize, Users us);
    void savedongtai(Dongtai dongtai, String imglist);
    //查询评论,包括一级和二级
    Map<String,Object>pinlunlist(String id);
}
