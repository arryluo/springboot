package com.arryluo.xinim.system.dao;


import com.arryluo.xinim.system.mode.Apppunlun_D;

import java.util.List;

/**
 * Created by LUOZUBANG on 2018/1/29.
 */
public interface AppPinlun_DMapper {
    //二级评论的添加
    void save_pinlun_d(Apppunlun_D pinLun_d);
    List<Apppunlun_D> selectbyoneid(String danjuid);
}
