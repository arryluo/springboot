package com.arryluo.xinim.system.dao;

import com.arryluo.xinim.system.mode.Fujian;


import java.util.List;

/**
 * Created by LUOZUBANG on 2018/1/21.
 */
public interface FujianMapper {
    List<Fujian>selectAll(String danjuid);
    void savefujian(Fujian fujian);
}
