package com.arryluo.xinim.system.dao;

import com.arryluo.xinim.comm.config.util.Pager;
import com.arryluo.xinim.system.mode.Dongtai;


import java.util.List;

/**
 * Created by LUOZUBANG on 2018/1/21.动态
 */
public interface DongtaiMapper {
    List<Dongtai> selectdongtai(Pager<Dongtai> pager);
    int count();
    void savedongtai(Dongtai dongtai);

}
