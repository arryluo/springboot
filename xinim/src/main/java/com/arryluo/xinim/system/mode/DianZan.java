package com.arryluo.xinim.system.mode;

import java.io.Serializable;

/**
 * Created by LUOZUBANG on 2018/2/5.
 */
public class DianZan implements Serializable {
    private String id;
    private String danjuid;
    private String uid;
    private String uname;
    private String createtime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDanjuid() {
        return danjuid;
    }

    public void setDanjuid(String danjuid) {
        this.danjuid = danjuid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }
}
