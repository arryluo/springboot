package com.arryluo.xinim.system.mode;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by LUOZUBANG on 2017/8/13.
 */
public class Users implements Serializable {
  private String id;
  private String username;
  private String pwd;
  private String img;
  private String type;
  private String createtime;
  private String nickname;


    public Users() {
    }

    public Users(String username, String pwd) {
        this.username = username;
        this.pwd = pwd;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
