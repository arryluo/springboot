package com.arryluo.xinim.system.mode;

import java.io.Serializable;

/**
 * Created by LUOZUBANG on 2018/1/21.
 */
public class Dongtai implements Serializable {
    private String id;
    private String uid;
    private String content;
    private String createtime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }
}
