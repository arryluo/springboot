package com.arryluo.xinim.system.mode;

import java.io.Serializable;

/**
 * Created by LUOZUBANG on 2018/1/29.
 */
public class Apppunlun_D implements Serializable {
    private String id;
    private String content;
    private String createtime;
    private String danjuid;
    private String zid;//这个是一级的id
    private String uid;
    private int flgtype;

    public int getFlgtype() {
        return flgtype;
    }

    public void setFlgtype(int flgtype) {
        this.flgtype = flgtype;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getDanjuid() {
        return danjuid;
    }

    public void setDanjuid(String danjuid) {
        this.danjuid = danjuid;
    }

    public String getZid() {
        return zid;
    }

    public void setZid(String zid) {
        this.zid = zid;
    }
}
