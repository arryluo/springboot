package com.arryluo.xinim.system.controller;



import com.alibaba.dubbo.common.utils.StringUtils;
import com.arryluo.xinim.comm.config.util.Const;
import com.arryluo.xinim.comm.config.util.DateUtil;
import com.arryluo.xinim.comm.config.util.UuidUtil;
import com.arryluo.xinim.system.dao.AppPinlunMapper;
import com.arryluo.xinim.system.dao.AppPinlun_DMapper;
import com.arryluo.xinim.system.dao.DianZanMapper;
import com.arryluo.xinim.system.dao.UserMapper;
import com.arryluo.xinim.system.mode.*;
import com.arryluo.xinim.system.service.DongtaiService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


/**
 * Created by LUOZUBANG on 2018/1/21.
 * App管理
 */
@Controller
public class AppManager {
    @Autowired
    private DongtaiService dongtaiService;
    //评论的添加
    @Autowired
    private AppPinlunMapper appPinlunMapper;
    @Autowired
    private AppPinlun_DMapper appPinlun_dMapper;
    @Autowired
    private UserMapper userMapper;
    //点赞
    @Autowired
    private DianZanMapper dianZanMapper;
   /* //访问动态
    @RequestMapping("dongtaiui")
    public String todongtaiui(String uid){
        return "/AppManager/dongt";
    }*/
    //进行文章的添加ui
    @RequestMapping("dongtaiaddui")
    public String adddt(String uid){

        return "/AppManager/adddt";
    }
    //查询动态数据,
    @RequestMapping("dtlist")
    @ResponseBody
    public Object contentdata(Integer pageno,String uid){
       // Users users=new Users();
       // users.setId("3c0caf4c4b8f476697fd91ab170047ba");
       // users.set
        Users users=null;
        //if(uid==null){
            Subject subject = SecurityUtils.getSubject();

            Session session= subject.getSession();
             users= (Users) session.getAttribute(Const.SESSION_USER);
        /*}else{
            users=userMapper.userinfo(uid);
        }*/

       return dongtaiService.dongtailist(pageno,2,users);
    }

    //查询评论
    @RequestMapping("pinlunlist")
    @ResponseBody
    public Object pinlunlist(String id){
        Map<String,Object>maps=dongtaiService.pinlunlist(id);
        System.out.println(maps);
        return maps;
    }
    //真的添加
    @RequestMapping("dtsave")
    @ResponseBody
    public String add(String content,String imglist,String uid){
        Dongtai dongtai=new Dongtai();
        //获取Session
       /*  Subject subject = SecurityUtils.getSubject();
        Session session= subject.getSession();
        Users users= (Users) session.getAttribute(Const.SESSION_USER);*/
      //先测试用，
        dongtai.setUid(uid);
        dongtai.setContent(content);
        dongtaiService.savedongtai(dongtai,imglist);
        return "save";
    }
    //添加评论操作
    @RequestMapping("addpinlun")
    @ResponseBody
    public String addpinlun(String uid,String danjuid,String content,String typeflg,String zid){
        //1是一级评论，2是二级评论
        //JSONObject jsonObject=new JSONObject(json);

        if(StringUtils.isEquals(typeflg,"1")){
            Apppunlun apppunlun=new Apppunlun();
           // String uid=jsonObject.optString("uid");
           // String danjuid=jsonObject.optString("danjuid");
           // String content=jsonObject.optString("content");
            apppunlun.setUid(uid);
            apppunlun.setId(UuidUtil.get32UUID());
            apppunlun.setDanjuid(danjuid);
            apppunlun.setContent(content);
            apppunlun.setCreatetime(DateUtil.getTime());
            appPinlunMapper.saveapppinlun(apppunlun);
        }else{
            Apppunlun_D pinLun_d=new Apppunlun_D();
            pinLun_d.setId(UuidUtil.get32UUID());
            pinLun_d.setCreatetime(DateUtil.getTime());
            pinLun_d.setContent(content);
            pinLun_d.setDanjuid(danjuid);
            pinLun_d.setZid(zid);
            pinLun_d.setUid(uid);
            pinLun_d.setFlgtype(1);
            appPinlun_dMapper.save_pinlun_d(pinLun_d);
        }
        return "code";
    }
    //点赞添加
    @RequestMapping("dainzan")
    @ResponseBody
    public String dianzan(DianZan dianZan, String flgtype){
        if("1".equals(flgtype)){
            dianZan.setId(UuidUtil.get32UUID());
            dianZan.setCreatetime(DateUtil.getTime());

            dianZanMapper.savedianzan(dianZan);
        }else if("2".equals(flgtype)){
            dianZanMapper.deldianzan(dianZan.getDanjuid());
        }

        return "code";
    }

}
