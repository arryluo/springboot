package com.arryluo.xinim.system.mode;

import java.io.Serializable;

/**
 * Created by LUOZUBANG on 2018/1/21.
 */
public class Fujian implements Serializable {
    private String id;
    private String uid;
    private String picurl;
    private String createtime;
    private String danjuid;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPicurl() {
        return picurl;
    }

    public void setPicurl(String picurl) {
        this.picurl = picurl;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getDanjuid() {
        return danjuid;
    }

    public void setDanjuid(String danjuid) {
        this.danjuid = danjuid;
    }
}
