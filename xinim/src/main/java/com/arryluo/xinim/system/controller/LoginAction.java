package com.arryluo.xinim.system.controller;

import com.arryluo.xinim.comm.config.util.JsonObject;
import com.arryluo.xinim.system.dao.UserMapper;
import com.arryluo.xinim.system.mode.Users;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


@Controller
public class LoginAction {
    @Autowired
    private UserMapper userMapper;

    @RequestMapping("/index")
    @ResponseBody
    public Object all(){

        return "";
    }
   //登录请求
    @RequestMapping("/login")
    @ResponseBody
    public JsonObject login(Users users){
        UsernamePasswordToken token = new UsernamePasswordToken(users.getUsername(), users.getPwd());
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(token);
            return JsonObject.ok();
        } catch (AuthenticationException e) {
            System.out.println(e.getMessage());
            return JsonObject.error(e.getMessage());
        }
       //return userMapper.login(users);
    }
}
