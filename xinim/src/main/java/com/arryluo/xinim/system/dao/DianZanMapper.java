package com.arryluo.xinim.system.dao;

import com.arryluo.xinim.system.mode.DianZan;


import java.util.List;
import java.util.Map;

/**
 * Created by LUOZUBANG on 2018/2/5.
 */
public interface DianZanMapper {
    void savedianzan(DianZan dianZan);
    void deldianzan(String danjuid);
    List<DianZan>dianzanbydanjuid(String danjuid);
    List<DianZan> dianjuinfo(Map<String, Object> map);
}
